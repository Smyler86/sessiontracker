package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import models.RecordedLocation;
import models.TrackSession;
import models.TrackedAction;
import nl.bitwalker.useragentutils.UserAgent;

import org.apache.commons.io.IOUtils;

import play.Play;
import play.data.Form;
import play.data.validation.Constraints.Required;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;
import utils.Base64;
import utils.Tools;

public class DataHub extends Controller {

	public static class TrackRequest {
		@Required
		public String d;
		@Required
		public String host;
		@Required
		public String key;
	}

	public static Result track() {

		response().setContentType("image/png");
		InputStream outGifStream = Play.application().resourceAsStream("/public/images/site/blank.png");
		byte[] outBytedGif;
		try {
			outBytedGif = IOUtils.toByteArray(outGifStream);
		} catch (IOException e1) {
			outBytedGif = new byte[] {};
			e1.printStackTrace();
		}

		SimpleDateFormat httpDateFormat = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
		int timeOffset = TimeZone.getDefault().getOffset(new Date().getTime()) + 7200000;
		java.util.Calendar cal = Calendar.getInstance(new SimpleTimeZone(0, "GMT"));
		httpDateFormat.setCalendar(cal);

		Form<TrackRequest> req = form(TrackRequest.class).bindFromRequest();

		if (req.hasErrors())
			return badRequest(outBytedGif);

		TrackSession.Model trackSess = null;
		String cookieSessionName = Tools.md5Encode(req.get().host) + "_sess";
		Long systemTs = new Date().getTime();

		Http.Cookie storedTrackedSessionId = request().cookies().get(cookieSessionName);
		StringBuilder toSetCookiesString = new StringBuilder();
		toSetCookiesString.append("_we3ctr=1" + "; Expires=" + httpDateFormat.format(new Date(systemTs + 3600000 + timeOffset)) + "; Path=/");

		if (storedTrackedSessionId != null) {
			trackSess = TrackSession.coll.findOneById(storedTrackedSessionId.value());
		}
		if (trackSess == null) {
			trackSess = new TrackSession.Model();
			trackSess.startedAt = new Date();
			if (Context.current().request().headers().containsKey("USER-AGENT") && Context.current().request().headers().get("USER-AGENT").length > 0) {
				trackSess.userAgent = Context.current().request().headers().get("USER-AGENT")[0];
				UserAgent userAgent = UserAgent.parseUserAgentString(trackSess.userAgent);
				trackSess.os = userAgent.getOperatingSystem().name();
				trackSess.browser = userAgent.getBrowser().name();
			}
			if (Context.current().request().headers().containsKey("ACCEPT-LANGUAGE") && Context.current().request().headers().get("ACCEPT-LANGUAGE").length > 0) {
				trackSess.language = Context.current().request().headers().get("ACCEPT-LANGUAGE")[0];
				String[] languages = trackSess.language.split(",");
				if (languages.length > 1)
					trackSess.mainLanguage = languages[0];
				else
					trackSess.mainLanguage = trackSess.language;
			}
			trackSess.host = req.get().host;
			;
			trackSess._id = TrackSession.save(trackSess).getSavedId();

			// TODO: get client IP using http proxy
		}

		toSetCookiesString.append("; " + cookieSessionName + "=" + trackSess._id + "; Expires="
				+ httpDateFormat.format(new Date(systemTs + 3600000 + timeOffset)) + "; Path=/");

		RecordedLocation.Model loc = null;
		String cookiesLocationName = Tools.md5Encode(req.get().host) + "_last_loc"; // last
																					// tracked
																					// location
		Http.Cookie lastTrackedLocationId = request().cookies().get(cookiesLocationName);

		if (lastTrackedLocationId != null) {
			loc = RecordedLocation.coll.findOneById(lastTrackedLocationId.value());
		}

		String actionsString = new String(Base64.decode(req.get().d));

		if (actionsString.length() < 5)
			badRequest(outGifStream);

		String[] actions = actionsString.split("}");
		Long lastTs = 0L;
		for (int i = 0; i < actions.length; i++) {
			String[] parts = actions[i].split("[|]");
			if (parts.length < 1)
				continue;
			TrackedAction.Model action = null;
			try {
				switch (Byte.valueOf(parts[0])) {
				case 0:
					if (parts.length != 7)
						continue;
					// TODO:Track domains and pageUrl
					action = new TrackedAction.Model();
					action.e = 0;
					action.location = parts[1];
					action.w = Short.valueOf(parts[2]);
					action.h = Short.valueOf(parts[3]);
					action.t = Short.valueOf(parts[4]);
					action.l = Short.valueOf(parts[5]);
					action.ts = Long.valueOf(parts[6]);
					loc = new RecordedLocation.Model();
					loc.sessionId = trackSess._id;
					loc.startedAt = new Date(action.ts);
					loc.location = parts[1];
					loc._id = RecordedLocation.save(loc).getSavedId();
					if (trackSess.firstActionAt == null) {
						trackSess.firstActionAt = new Date(action.ts);
						TrackSession.save(trackSess);
					}
					toSetCookiesString.append("; " + cookiesLocationName + "=" + loc._id + "; Expires="
							+ httpDateFormat.format(new Date(systemTs + 3600000 + timeOffset)) + "; Path=/");

					break;
				case 1: // mouse down
					// TODO: inspect errors and cases here
					if (loc == null)
						return badRequest(outBytedGif);
					if (parts.length != 6)
						continue;
					action = new TrackedAction.Model();
					action.e = 1;
					action.x = Short.valueOf(parts[1]);
					action.y = Short.valueOf(parts[2]);
					action.w = Short.valueOf(parts[3]);
					action.h = Short.valueOf(parts[4]);
					action.ts = Long.valueOf(parts[5]);
					break;
				case 2: // move
					// TODO: inspect errors and cases here
					if (loc == null)
						return badRequest(outBytedGif);
					if (parts.length != 6)
						continue;
					action = new TrackedAction.Model();
					action.e = 2;
					action.x = Short.valueOf(parts[1]);
					action.y = Short.valueOf(parts[2]);
					action.w = Short.valueOf(parts[3]);
					action.h = Short.valueOf(parts[4]);
					action.ts = Long.valueOf(parts[5]);
					break;
				case 3: // resize
					// TODO: inspect errors and cases here
					if (loc == null)
						return badRequest(outBytedGif);
					if (parts.length != 4)
						continue;
					action = new TrackedAction.Model();
					action.e = 3;
					action.w = Short.valueOf(parts[1]);
					action.h = Short.valueOf(parts[2]);
					action.ts = Long.valueOf(parts[3]);
					break;
				case 4: // scroll
					// TODO: inspect errors and cases here
					if (loc == null)
						return badRequest(outBytedGif);
					if (parts.length != 5)
						continue;
					action = new TrackedAction.Model();
					action.e = 4;
					action.t = Short.valueOf(parts[1]);
					action.l = Short.valueOf(parts[2]);
					action.d = parts[3];
					action.ts = Long.valueOf(parts[4]);
					break;
				case 5:
					break;
				}
			} catch (NumberFormatException e) {
				continue;
			}

			if (action != null) {
				action.recLocId = loc._id;
				TrackedAction.save(action);
				lastTs = action.ts;
			}
		}
		if (lastTs > 0) {
			loc.lastActionAt = new Date(lastTs);
			RecordedLocation.save(loc);
			trackSess.lastActionAt = new Date(lastTs);
			TrackSession.save(trackSess);
		}

		if (toSetCookiesString.length() > 1) {
			response().setHeader("p3p", "CP=\"CAO PSA OUR\"");
			response().setHeader("Set-Cookie",
					toSetCookiesString + "; Expires=" + httpDateFormat.format(new Date(systemTs + 3600000 + timeOffset)) + "; Path=/");
		}
		return ok(outBytedGif);

	}

}
